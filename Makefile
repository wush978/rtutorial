# .PHONY: bw_r_tutorial r_importdata 

all: bw_r_tutorial r_importdata r_datastructure

bw_r_tutorial:
	$(MAKE) -C Bridgewell-R-Tutorial

r_importdata:
	$(MAKE) -C R_ImportData

r_datastructure
	$(MAKE) -C R_DataStructure

clean:
	$(MAKE) -C Bridgewell-R-Tutorial clean
	$(MAKE) -C R_ImportData clean
	$(MAKE) -C R_DataStructure clean
